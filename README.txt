Current - Curriculum Entry System

BUILDING
This was built using Maven 3.3.9 and Java 1.8.0_71
To build the distributable .jar file...
1. cd into the server directory
2. Run 'mvn install'
3. Run 'mvn package'
4. The .jar file is now at the root of the target/ directory.

RUNNING
The command to run the server (from the .jar file) is as follows (YMMV):
java -jar current-0.1.0.jar --spring.config.location=config/prod.yml

CONFIGURATION
Certain configurations ARE required in the configuration file. Most failures that occur as the server is starting can usually be attributed to either a bad config file, an error connecting to the configured database, or another process already using the port you are trying

'use strict';

angular.module('controllers')
.controller('TopicsCtrl', ['$scope', '$http', '$mdDialog', '$location', 'NgTableParams',
    function ($scope, $http, $mdDialog, $location, NgTableParams) {

        var refreshTable = function () {
            $scope.tableParams.reload();
        };

        $scope.tableParams = new NgTableParams({

        }, {
            counts: [],
            getData: function (params) {
                var filter = params.filter().description;
                var request = {
                    method: 'GET',
                    url: '/rest/ui/topic?filter=' + (filter ? filter : '*')
                };
                return $http(request).then(function (data) {
                    return data.data;
                });
            }
        });

        $scope.confirmDelete = function (topic) {
            var dialog = {
                htmlContent: '<h3>Delete topic ' +
                             '<span class="label label-topic">' + topic.description + '</span> ?</h3>',
                ok: 'DELETE',
                cancel: 'CANCEL'
            };
            var confirm = $mdDialog.confirm(dialog);
            $mdDialog.show(confirm).then(function () {
                deleteTopic(topic);
            }, {});
        };

        var deleteTopic = function (topic) {
            var request = {
                method: 'DELETE',
                url: '/rest/ui/topic',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: topic
            };
            $http(request).then(function () {
                refreshTable();
            });
        };

        $scope.newTopic = function () {
            $mdDialog.show({
                templateUrl: 'app/components/topic/new_topic_dialog.tmpl.html',
                controller: 'NewTopicCtrl'
            })
            .then(function (topicId) {
                $location.url('/topic/' + topicId);
            }, function () {

            });
        };

        $scope.$on('update', function () {
            refreshTable();
        });
    }]);

angular.module('controllers')
    .controller('NewTopicCtrl', ['$scope', '$http', '$mdDialog',
        function ($scope, $http, $mdDialog) {
            $scope.save = function () {
                var request = {
                    method: 'POST',
                    url: '/rest/ui/topic',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.model
                };
                $http(request)
                .success(function (topic) {
                    $mdDialog.hide(topic.id);
                });
            };
            
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
        }]);

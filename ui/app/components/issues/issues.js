'use strict';

angular.module('controllers')
.controller('IssuesCtrl', ['$scope', '$http',
        function ($scope, $http) {
            var getIssues = function () {
                var request = {
                    method: 'GET',
                    url: '/app/components/issues/issues.json'
                };
                $http(request)
                .success(function (data) {
                    $scope.issues = data;
                });
            };
            getIssues();
        }]);

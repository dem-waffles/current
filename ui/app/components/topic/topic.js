angular.module('controllers')
    .controller('TopicCtrl', ['$scope', '$http', '$routeParams', '$interpolate', 'NgTableParams',
            function ($scope, $http, $routeParams, $interpolate, NgTableParams) {
                var getTopic = function () {
                    var request = { 
                        method: 'GET',
                        url: '/rest/ui/topic/' + $routeParams.topicId
                    };
                    $http(request)
                    .success(function (data) {
                        $scope.model = data;
                    });
                };
                $scope.delete = function (course) {
                    var request = {
                        method: 'POST',
                        url: '/rest/ui/topic/' + $routeParams.topicId + '/disassociate/' + course.id
                    };
                    $http(request)
                    .success(function () {
                        $scope.associatedCoursesParams.reload();
                        $scope.unassociatedCoursesParams.reload();
                    });
                };
                $scope.associate = function (course) {
                    var request = {
                        method: 'POST',
                        url: '/rest/ui/topic/' + $routeParams.topicId + '/associate/' + course.id
                    };
                    $http(request)
                    .success(function () {
                        $scope.associatedCoursesParams.reload();
                        $scope.unassociatedCoursesParams.reload();
                    });
                };
                
                $scope.save = function () {
                    var request = {
                        method: 'POST',
                        url: '/rest/ui/topic',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        data: $scope.model
                    };
                    return $http(request)
                    .success(function () {
                        $scope.edit = false;
                        getTopic();
                    });
                };
                
                $scope.associatedCoursesParams = new NgTableParams({}, {
                    counts: [],
                    getData: function (params) {
                        var courseNumberFilter = params.filter().courseId || '*';
                        var courseTitleFilter = params.filter().courseTitle || '*';
                        var context = {
                            courseNumberFilter: courseNumberFilter,
                            courseTitleFilter: courseTitleFilter,
                            topicId: $routeParams.topicId
                        };
                        var request = {
                            method: 'GET',
                            url: $interpolate('/rest/ui/topic/{{topicId}}/courses?courseNumberFilter={{courseNumberFilter}}&courseTitleFilter={{courseTitleFilter}}')(context)
                        };
                        return $http(request).then(function (data) {
                            return $scope.courses = data.data;
                        });
                    }
                });

                $scope.unassociatedCoursesParams = new NgTableParams({}, {
                    counts: [],
                    getData: function (params) {
                        var courseNumberFilter = params.filter().courseId || '*';
                        var courseTitleFilter = params.filter().courseTitle || '*';
                        var context = {
                            courseNumberFilter: courseNumberFilter,
                            courseTitleFilter: courseTitleFilter,
                            topicId: $routeParams.topicId
                        };
                        var request = {
                            method: 'GET',
                            url: $interpolate('/rest/ui/course/unassociated?topicId={{topicId}}&courseNumberFilter={{courseNumberFilter}}&courseTitleFilter={{courseTitleFilter}}')(context)
                        };
                        return $http(request).then(function (data) {
                            return $scope.courses = data.data;
                        });
                    }
                });

                getTopic();
            }]);

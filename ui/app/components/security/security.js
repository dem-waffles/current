'use strict';

angular.module('services')
.factory('Security', ['$http', '$interpolate', '$location',
        function ($http, $interpolate, $location) {
            var currentUser = null;

            var login = function (credentials) {
                var request = {
                    method: 'POST',
                    url: '/security/login',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: $interpolate('username={{username}}&password={{password}}')(credentials)
                };
                return $http(request)
                    .success(function () {
                        currentUser = 'not-null';
                    });
            };

            var logout = function () {
                var request = {
                    method: 'POST',
                    url: '/security/logout'
                };
                $http(request)
                    .success(function () {
                        currentUser = null;
                        $location.url('current/login');
                    });
            };

            var requestCurrentUser = function () {
                var request = {
                    method: 'GET',
                    url: '/rest/ui/auth/currentUser'
                };
                return $http(request)
                .success(function (data) {
                    currentUser = data;
                });
            };

            return {
                login: function (credentials) {
                    return login(credentials);
                },
                logout: function () {
                    logout();
                },
                getCurrentUser: function () {
                    return currentUser;
                },
                requestCurrentUser: function () {
                    return requestCurrentUser();
                },
                isAuthenticated: function () {
                    return !!currentUser;
                }
            };
    }]);

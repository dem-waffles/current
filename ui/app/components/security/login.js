'use strict';

angular.module('controllers')
    .controller('LoginCtrl', ['$scope', '$location', 'Security', 'ServerDetails',
        function ($scope, $location, Security, ServerDetails) {
            $scope.login = function () {
                Security.login($scope.model)
                .success(function () {
                    $location.url('current/courses');
                });
            };
        }]);

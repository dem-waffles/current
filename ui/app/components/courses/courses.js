'use strict';

angular.module('controllers')
.controller('CoursesCtrl', ['$scope', '$http', '$interpolate', '$mdDialog', '$location', 'NgTableParams',
    function ($scope, $http, $interpolate, $mdDialog, $location, NgTableParams) {

        var refreshTable = function () {
            $scope.tableParams.reload();
        };

        $scope.tableParams = new NgTableParams({}, {
            counts: [],
            getData: function (params) {
                var courseNumberFilter = params.filter().courseId || '*';
                var courseTitleFilter = params.filter().courseTitle || '*';
                var context = {
                    courseNumberFilter: courseNumberFilter,
                    courseTitleFilter: courseTitleFilter
                };
                var request = {
                    method: 'GET',
                    url: $interpolate('/rest/ui/course?courseNumberFilter={{courseNumberFilter}}&courseTitleFilter={{courseTitleFilter}}')(context)
                };
                return $http(request).then(function (data) {
                    return data.data;
                });
            }
        });

        $scope.confirmDelete = function (course) {
            var dialog = {
                htmlContent: '<h3>Delete course ' +
                '<span class="label label-course">' + course.courseNumber + ': ' + course.courseTitle + '</span> ?</h3>',
                ok: 'DELETE',
                cancel: 'CANCEL'
            };
            var confirm = $mdDialog.confirm(dialog);
            $mdDialog.show(confirm).then(function () {
                deleteCourse(course);
            }, {});
        };

        var deleteCourse = function (course) {
            var request = {
                method: 'DELETE',
                url: '/rest/ui/course',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: course
            };
            $http(request).then(function () {
                refreshTable();
            });
        };

        $scope.newCourse = function () {
            $mdDialog.show({
                templateUrl: 'app/components/course/new_course_dialog.tmpl.html',
                controller: 'NewCourseCtrl'
            })
            .then(function(courseId) {
                $location.url('/course/' + courseId);
            }, function () {

            });
        };

        $scope.$on('update', function () {
            refreshTable();
        });
    }]);

angular.module('controllers')
    .controller('NewCourseCtrl', ['$scope', '$http', '$mdDialog',
        function ($scope, $http, $mdDialog) {
            $scope.save = function () {
                var request = {
                    method: 'POST',
                    url: '/rest/ui/course',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    data: $scope.model
                };
                $http(request)
                .success(function (course) {
                    $mdDialog.hide(course.id);
                });
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };
        }]);

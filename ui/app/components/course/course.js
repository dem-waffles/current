angular.module('controllers')
.controller('CourseCtrl', ['$scope', '$http', '$routeParams', '$interpolate', 'NgTableParams',
    function ($scope, $http, $routeParams, $interpolate, NgTableParams) {
        $scope.$routeParams = $routeParams;
        var getCourse = function () {
            var request = {
                method: 'GET',
                url: '/rest/ui/course/' + $routeParams.courseId
            };
            $http(request)
            .success(function (data) {
                $scope.model = data;
            });
        };
        $scope.delete = function (topic) {
            var request = {
                method: 'POST',
                url: '/rest/ui/course/' + $routeParams.courseId + '/disassociate/' + topic.id
            };
            $http(request)
            .success(function () {
                $scope.associatedTopicsParams.reload();
                $scope.unassociatedTopicsParams.reload();
            });
        };
        $scope.associate = function (topic) {
            var request = {
                method: 'POST',
                url: '/rest/ui/course/' + $routeParams.courseId + '/associate/' + topic.id
            };
            $http(request)
            .success(function () {
                $scope.associatedTopicsParams.reload();
                $scope.unassociatedTopicsParams.reload();
            });
        };

        $scope.save = function () {
            var request = {
                method: 'POST',
                url: '/rest/ui/course',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: $scope.model
            };
            return $http(request)
            .success(function () {
                $scope.edit = false;
                getCourse();
            });
        };

        $scope.associatedTopicsParams = new NgTableParams({}, {
            counts: [],
            getData: function (params) {
                var filter = params.filter().description;
                var request = {
                    method: 'GET',
                    url: '/rest/ui/course/' + $routeParams.courseId + '/topics?filter=' + (filter ? filter : '*')
                };
                return $http(request).then(function (data) {
                    $scope.unassociatedTopicsParams.reload();
                    return data.data;
                });
            }
        });

        $scope.unassociatedTopicsParams = new NgTableParams({}, {
            counts: [],
            getData: function (params) {
                var filter = params.filter().description;
                var context = {
                    courseId: $routeParams.courseId,
                    filter: filter ? filter : '*'
                };
                var request = {
                    method: 'GET',
                    url: $interpolate('/rest/ui/topic/unassociated?courseId={{courseId}}&filter={{filter}}')(context)
                };
                return $http(request).then(function (data) {
                    return data.data;
                });
            }
        });

        getCourse();

    }]);

'use strict';

angular.module('controllers')
    .controller('SandboxCtrl', ['$scope', '$http', '$rootScope', '$mdDialog', 'Security', 'ServerDetails',
            function ($scope, $http, $rootScope, $mdDialog, Security, ServerDetails) {
                var loadServerDetails = function () {
                    ServerDetails.loadServerDetails()
                    .success(function () {
                        $scope.isSandbox = ServerDetails.getServerDetails().profiles.indexOf('sandbox') !== -1;
                    })
                    .error(function () {
                        $scope.isSandbox = false;
                    });
                };
                $rootScope.$on('$locationChangeSuccess', function () {
                    loadServerDetails();
                });
                loadServerDetails();
                $scope.confirmWipeData = function ($event) {
                    var dialog = {
                        title: 'Warning',
                        textContent: 'Are you sure you want to delete DEV data from H2?',
                        ok: 'YES',
                        cancel: 'NO',
                        targetEvent: $event
                    };
                    $mdDialog.show($mdDialog.confirm(dialog)).then(function () {
                        wipeData();
                    });
                };

                var wipeData = function () {
                    var request = {
                        method: 'POST',
                        url: '/rest/ui/sandbox/wipe'
                    };
                    $http(request).then(function () {
                        $rootScope.$emit('update');
                        $rootScope.$broadcast('update');
                    });
                };
                $scope.populateData = function () {
                    var request = {
                        method: 'POST',
                        url: '/rest/ui/sandbox/populate'
                    };
                    $http(request).then(function () {
                        $rootScope.$emit('update');
                        $rootScope.$broadcast('update');
                    });
                };
            }])
    .directive('sandboxTools', function () {
        return {
            templateUrl: 'app/components/sandbox/sandbox.html',
            controller: 'SandboxCtrl'
        };
    });

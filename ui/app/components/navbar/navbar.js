'use strict';

// Add the NavbarCtrl to the controllers module we created in app.js.
angular.module('controllers')
    .controller('NavbarCtrl', ['$scope', 'Security',
            function ($scope, Security) {
                $scope.Security = Security;
            }])
    // Angular parses the HTML slashes and converts to camel case.
    // so, my-navbar will map to myNavbar, and Angular wires that
    // HTML directive to this Angular module.
    .directive('myNavbar', function () {
        return {
            templateUrl: 'app/components/navbar/navbar.html'
        };
    });

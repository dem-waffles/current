'use strict';

angular.module('services')
.factory('httpRequestInterceptor', function ($q, $rootScope) {
    return {
        'responseError': function(rejection) {
            if(rejection.status === 401){
                $rootScope.$broadcast('unauthorized');
            } else {
                $rootScope.$broadcast('error', rejection);
            }
            return $q.reject(rejection);
         }
     };
});

angular.module('controllers')
.controller('ErrorToastCtrl', ['$rootScope', '$mdToast', 'Security',
    function ($rootScope, $mdToast, Security) {
        $rootScope.$on('unauthorized', function () {
            Security.logout();
        });
        $rootScope.$on('error', function (event, data) {
            var toast = $mdToast.simple()
                        .content(data.data.message)
                        .position('top right')
                        .theme('danger-toast');
            $mdToast.show(toast);
            });
    }]);
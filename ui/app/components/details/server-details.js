'use strict';

angular.module('services')
.factory('ServerDetails', ['$http', function ($http) {
    var serverDetails;
    return {
        loadServerDetails: function () {
            var request = {
                method: 'GET',
                url: '/rest/ui/serverDetails'
            };
            return $http(request)
            .success(function (data) {
                serverDetails = data;
            });
        },
        getServerDetails: function () {
            return serverDetails;
        }
    };
}]);
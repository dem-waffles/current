'use strict';

// Build a module to store services. It will have no dependencies.
angular.module('services', []);
// Build a module to store controllers. Controllers use services, so we inject that as a dependency.
angular.module('controllers', ['services']);
// Build the angular instance of our app and let it use our controllers.
angular.module('MinesCrud', [
   'controllers',
   'ngRoute',
   'ngTable',
   'ngMaterial',
   'ngSanitize'
]);

angular.module('MinesCrud')
    .config(['$routeProvider', '$httpProvider',
            function ($routeProvider, $httpProvider) {
                $routeProvider
                .when('/', {
                    redirectTo: 'current/courses'
                })
                .when('/current/topics', {
                    templateUrl: 'app/components/topics/topics.html'
                })
                .when('/current/courses', {
                    templateUrl: 'app/components/courses/courses.html'
                })
                .when('/current/course/:courseId', {
                    templateUrl: 'app/components/course/course.html'
                })
                .when('/current/topic/:topicId', {
                    templateUrl: 'app/components/topic/topic.html'
                })
                .when('/current/login', {
                    templateUrl: 'app/components/security/login.html'
                })
                .when('/current/issues', {
                    templateUrl: 'app/components/issues/issues.html'
                })
                .otherwise({
                    redirectTo: '/current/courses'
                });

                $httpProvider.interceptors.push('httpRequestInterceptor');
            }]);

angular.module('MinesCrud')
    .run(['$location','Security', 'ServerDetails',
        function ($location, Security, ServerDetails) {
                    Security.requestCurrentUser().finally(function () {
                        if (Security.isAuthenticated()) {
                            $location.url('current/courses');
                        } else {
                            $location.url('current/login');
                        }
                    });
        }]);

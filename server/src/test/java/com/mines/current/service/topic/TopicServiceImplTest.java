package com.mines.current.service.topic;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;
import com.mines.current.repository.topic.TopicRepository;
import com.mines.current.service.course.CourseService;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;


public class TopicServiceImplTest {

    @Tested
    private TopicServiceImpl topicServiceImpl;

    @Injectable
    private TopicRepository topicRepository;

    @Injectable
    private CourseService courseService;

    private Topic topic1 = new Topic();
    private Topic topic2 = new Topic();
    private Topic topic3 = new Topic();

    @Before
    public void setUp() {
        topic1.setDescription("sql statements");
        topic2.setDescription("pointers and junk");
        topic3.setDescription("regular expressions");
    }

    @Test
    public void testGetAllTopics() throws Exception {

        // test the correct number of results
        new Expectations() {{
            topicRepository.findAll(); result = ImmutableSet.of(topic1, topic2, topic3);
        }};
        assertEquals(3, topicServiceImpl.getAllTopics().size());

        // test sorting
        new Expectations() {{
            topicRepository.findAll(); result = ImmutableSet.of(topic1, topic2, topic3);
        }};
        assertEquals(topic2, ((List) topicServiceImpl.getAllTopics()).get(0));

        new Expectations() {{
            topicRepository.findAll(); result = ImmutableSet.of(topic1, topic2, topic3);
        }};
        assertEquals(topic3, ((List) topicServiceImpl.getAllTopics()).get(1));

        new Expectations() {{
            topicRepository.findAll(); result = ImmutableSet.of(topic1, topic2, topic3);
        }};
        assertEquals(topic1,  ((List) topicServiceImpl.getAllTopics()).get(2));
    }

    @Test
    public void testSaveTopic() throws Exception {
        new Expectations() {{
            topicRepository.findTopicByDescription(topic1.getDescription());
            result = null;
        }};
        topicServiceImpl.saveTopic(topic1);
        new Verifications() {{
            Topic topic;
            topicRepository.save(topic = withCapture());
            assertEquals(topic1.getId(), topic.getId());
        }};
    }

    @Test
    public void testGetEmptyTopic() throws Exception {
        new Expectations() {{
            topicRepository.findTopicByDescription(anyString);
            result = null;
        }};
        Optional<Topic> blah = topicServiceImpl.findTopic("blah");
        assertFalse(blah.isPresent());
    }

    @Test
    public void testFindTopic() throws Exception {
        final String TOPIC_DESCRIPTION = "sql statements";
        new Expectations() {{
            topicRepository.findTopicByDescription(TOPIC_DESCRIPTION);
            result = topic1;
        }};
        Optional<Topic> topic = topicServiceImpl.findTopic(TOPIC_DESCRIPTION);
        assertTrue(topic.isPresent());
        assertEquals(TOPIC_DESCRIPTION, topic.get().getDescription());
    }

    @Test
    public void testDisassociateCourse() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("4d5d0f5e-bdf7-4095-9eea-e77fd1cba7db");
        final UUID COURSE_ID = UUID.fromString("bf5c2459-dcba-4b72-b169-4b0d64ee0152");

        Course course = new Course();
        course.setId(COURSE_ID);
        Topic topic = new Topic();
        topic.setId(TOPIC_ID);
        topic.setCourses(Sets.newHashSet(course));

        new Expectations() {{
            topicRepository.findOne(TOPIC_ID);
            result = topic;
            courseService.findCourse(COURSE_ID);
            result = Optional.of(course);
        }};

        topicServiceImpl.disassociateCourse(TOPIC_ID, COURSE_ID);

        // this block verifies the correct calls with the correct parameters get made to the database.
        // in this case, the topic being saved should have an empty collection of courses, where it previously
        // had one course.
        new Verifications() {{
            Topic capturedTopic;
            topicRepository.save(capturedTopic = withCapture());
            assertTrue(capturedTopic.getCourses().isEmpty());
        }};
    }

    @Test
    public void testAssociateCourse() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("4d5d0f5e-bdf7-4095-9eea-e77fd1cba7db");
        final UUID COURSE_ID = UUID.fromString("bf5c2459-dcba-4b72-b169-4b0d64ee0152");

        Course course = new Course();
        course.setId(COURSE_ID);
        Topic topic = new Topic();
        topic.setId(TOPIC_ID);

        new Expectations() {{
            topicRepository.findOne(TOPIC_ID);
            result = topic;
            courseService.findCourse(COURSE_ID);
            result = Optional.of(course);
        }};

        topicServiceImpl.associateCourse(TOPIC_ID, COURSE_ID);

        new Verifications() {{
            Topic capturedTopic;
            topicRepository.save(capturedTopic = withCapture());
            assertEquals(1, capturedTopic.getCourses().size());
            assertEquals(COURSE_ID, capturedTopic.getCourses().iterator().next().getId());
        }};
    }

    @Test
    public void testGetAssociatedCoursesNullFilters() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("4d5d0f5e-bdf7-4095-9eea-e77fd1cba7db");
        final UUID COURSE_ID_1 = UUID.fromString("bf5c2459-dcba-4b72-b169-4b0d64ee0152");
        final UUID COURSE_ID_2 = UUID.fromString("9b9cf7f0-5abf-4b96-b78d-9836644df50c");

        Course course1 = new Course();
        course1.setId(COURSE_ID_1);
        Course course2 = new Course();
        course2.setId(COURSE_ID_2);
        Topic topic = new Topic();
        topic.setId(TOPIC_ID);
        topic.setCourses(Sets.newHashSet(course1, course2));

        new Expectations() {{
           topicRepository.findOne(TOPIC_ID);
           result = topic;
        }};

        Collection<Course> associatedCourses = topicServiceImpl.getAssociatedCourses(TOPIC_ID, null, null);

        assertEquals(2, associatedCourses.size());
    }

    @Test
    public void testGetAssociatedCoursesWildcardFilters() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("4d5d0f5e-bdf7-4095-9eea-e77fd1cba7db");
        final UUID COURSE_ID_1 = UUID.fromString("bf5c2459-dcba-4b72-b169-4b0d64ee0152");
        final UUID COURSE_ID_2 = UUID.fromString("9b9cf7f0-5abf-4b96-b78d-9836644df50c");

        Course course1 = new Course();
        course1.setId(COURSE_ID_1);
        course1.setCourseNumber("CSCI-101");
        course1.setCourseTitle("Intro to Computer Science");
        Course course2 = new Course();
        course2.setId(COURSE_ID_2);
        course2.setCourseNumber("CSCI-361");
        course2.setCourseTitle("Software Engineering");
        Topic topic = new Topic();
        topic.setId(TOPIC_ID);
        topic.setCourses(Sets.newHashSet(course1, course2));

        new Expectations() {{
            topicRepository.findOne(TOPIC_ID);
            result = topic;
        }};

        Collection<Course> associatedCourses = topicServiceImpl.getAssociatedCourses(TOPIC_ID, null, null);

        assertEquals(2, associatedCourses.size());
    }

    @Test
    public void testGetAssociatedCoursesRealFilter_courseNumber() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("4d5d0f5e-bdf7-4095-9eea-e77fd1cba7db");
        final UUID COURSE_ID_1 = UUID.fromString("bf5c2459-dcba-4b72-b169-4b0d64ee0152");
        final UUID COURSE_ID_2 = UUID.fromString("9b9cf7f0-5abf-4b96-b78d-9836644df50c");

        Course course1 = new Course();
        course1.setId(COURSE_ID_1);
        course1.setCourseNumber("CSCI-101");
        course1.setCourseTitle("Intro to Computer Science");
        Course course2 = new Course();
        course2.setId(COURSE_ID_2);
        course2.setCourseNumber("CSCI-361");
        course2.setCourseTitle("Software Engineering");
        Topic topic = new Topic();
        topic.setId(TOPIC_ID);
        topic.setCourses(Sets.newHashSet(course1, course2));

        new Expectations() {{
            topicRepository.findOne(TOPIC_ID);
            result = topic;
        }};

        Collection<Course> associatedCourses = topicServiceImpl.getAssociatedCourses(TOPIC_ID, "101", "*");

        assertEquals(1, associatedCourses.size());
    }

    @Test
    public void testGetAssociatedCoursesRealFilter_courseTitle() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("4d5d0f5e-bdf7-4095-9eea-e77fd1cba7db");
        final UUID COURSE_ID_1 = UUID.fromString("bf5c2459-dcba-4b72-b169-4b0d64ee0152");
        final UUID COURSE_ID_2 = UUID.fromString("9b9cf7f0-5abf-4b96-b78d-9836644df50c");

        Course course1 = new Course();
        course1.setId(COURSE_ID_1);
        course1.setCourseNumber("CSCI-101");
        course1.setCourseTitle("Intro to Computer Science");
        Course course2 = new Course();
        course2.setId(COURSE_ID_2);
        course2.setCourseNumber("CSCI-361");
        course2.setCourseTitle("Software Engineering");
        Topic topic = new Topic();
        topic.setId(TOPIC_ID);
        topic.setCourses(Sets.newHashSet(course1, course2));

        new Expectations() {{
            topicRepository.findOne(TOPIC_ID);
            result = topic;
        }};

        Collection<Course> associatedCourses = topicServiceImpl.getAssociatedCourses(TOPIC_ID, "*", "Computer");

        assertEquals(1, associatedCourses.size());
    }

    @Test
    public void testGetAllUnassociatedTopicsWildcardFilter() throws Exception {
        final UUID COURSE_ID = UUID.fromString("ef34e3da-aedd-4f46-8619-fde4153431ef");

        Topic topic = new Topic();
        topic.setDescription("2's complement integer representation");

        new Expectations() {{
            topicRepository.findAllUnassociatedTopics(COURSE_ID.toString());
            result = Sets.newHashSet(topic);
        }};
        Collection<Topic> allUnassociatedTopics = topicServiceImpl.getAllUnassociatedTopics(COURSE_ID, "*");
        assertEquals(1, allUnassociatedTopics.size());
    }

    @Test
    public void testGetAllUnassociatedTopicsRealFilter1() throws Exception {
        final UUID COURSE_ID = UUID.fromString("ef34e3da-aedd-4f46-8619-fde4153431ef");

        Topic topic1 = new Topic();
        topic1.setDescription("2's complement integer representation");

        Topic topic2 = new Topic();
        topic2.setDescription("Digital logic  (AND, OR, NOT, NAND, NOR, XOR)");

        new Expectations() {{
            topicRepository.findAllUnassociatedTopics(COURSE_ID.toString());
            result = Sets.newHashSet(topic1, topic2);
        }};
        Collection<Topic> allUnassociatedTopics = topicServiceImpl.getAllUnassociatedTopics(COURSE_ID, "complement");
        assertEquals(1, allUnassociatedTopics.size());
        assertEquals(topic1.getDescription(), allUnassociatedTopics.iterator().next().getDescription());
    }

    @Test
    public void testGetAllUnassociatedTopicsRealFilter2() throws Exception {
        final UUID COURSE_ID = UUID.fromString("ef34e3da-aedd-4f46-8619-fde4153431ef");

        Topic topic1 = new Topic();
        topic1.setDescription("2's complement integer representation");

        Topic topic2 = new Topic();
        topic2.setDescription("Digital logic  (AND, OR, NOT, NAND, NOR, XOR)");

        new Expectations() {{
            topicRepository.findAllUnassociatedTopics(COURSE_ID.toString());
            result = Sets.newHashSet(topic1, topic2);
        }};
        Collection<Topic> allUnassociatedTopics = topicServiceImpl.getAllUnassociatedTopics(COURSE_ID, "NAND");
        assertEquals(1, allUnassociatedTopics.size());
        assertEquals(topic2.getDescription(), allUnassociatedTopics.iterator().next().getDescription());
    }
}
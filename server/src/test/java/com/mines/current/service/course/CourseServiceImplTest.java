package com.mines.current.service.course;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.mines.current.error.InvalidCourseNumberException;
import com.mines.current.error.PropertyRequiredException;
import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;
import com.mines.current.repository.course.CourseRepository;
import com.mines.current.service.topic.TopicService;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class CourseServiceImplTest {

    private Course course1 = new Course();
    private Course course2 = new Course();
    private Course course3 = new Course();

    private Topic topic1 = new Topic();
    private Topic topic2 = new Topic();
    private Topic topic3 = new Topic();

    @Tested
    private CourseServiceImpl courseServiceImpl;

    @Injectable
    private CourseRepository courseRepository;

    @Injectable
    private TopicService topicService;

    @Before
    public void setUp() {
        course1.setCourseNumber("CSCI-403");
        course1.setCourseTitle("Database Management");
        course2.setCourseNumber("CSCI-262");
        course2.setCourseTitle("Data Structures");
        course3.setCourseNumber("CSCI-498");
        course3.setCourseTitle("Compiler Design");
        topic1.setDescription("sql statements");
        topic2.setDescription("pointers and junk");
        topic3.setDescription("regular expressions");
    }

    @Test
    public void testFindAllCourses() throws Exception {
        // test the correct number of results
        new Expectations() {{
            courseRepository.findAll(); result = ImmutableSet.of(course1, course2, course3);
        }};
        assertEquals(3, courseServiceImpl.getAllCourses().size());

        // test sorting
        new Expectations() {{
            courseRepository.findAll(); result = ImmutableSet.of(course1, course2, course3);
        }};
        assertEquals(course3, ((List) courseServiceImpl.getAllCourses()).get(0));

        new Expectations() {{
            courseRepository.findAll(); result = ImmutableSet.of(course1, course2, course3);
        }};
        assertEquals(course2, ((List) courseServiceImpl.getAllCourses()).get(1));

        new Expectations() {{
            courseRepository.findAll(); result = ImmutableSet.of(course1, course2, course3);
        }};
        assertEquals(course1, ((List) courseServiceImpl.getAllCourses()).get(2));

    }

    @Test
    public void testSaveCourse() throws Exception {
        new Expectations() {{
            courseRepository.findByCourseNumber(course1.getCourseNumber());
            result = null;
        }};
        courseServiceImpl.saveCourse(course1);
        new Verifications() {{
            Course course;
            courseRepository.save(course = withCapture());
            assertEquals(course1.getId(), course.getId());
        }};
    }

    @Test
    public void testCorrectCourseId() throws Exception {
        assertEquals("CSCI-999", courseServiceImpl.correctCourseNumber("csci-999"));
    }

    @Test(expected = InvalidCourseNumberException.class)
    public void testCorrectCourseIdNegative() throws Exception {
        courseServiceImpl.correctCourseNumber("blah");
    }

    /**
     * Make sure a user can't attempt to save a course with no courseId and courseTitle
     * @throws Exception
     */
    @Test(expected = PropertyRequiredException.class)
    public void testSaveEmptyCourse() throws Exception {
        Course course = new Course();
        courseServiceImpl.saveCourse(course);
    }

    /**
     * Make sure a user can't attempt to save a course with no courseTitle
     * @throws Exception
     */
    @Test(expected = PropertyRequiredException.class)
    public void testSaveCourseWithNo_courseTitle() throws Exception {
        Course course = new Course();
        course.setCourseNumber("CSCI-403");
        courseServiceImpl.saveCourse(course);
    }

    /**
     * Make sure a user can't attempt to save a course with no courseId
     * @throws Exception
     */
    @Test(expected = PropertyRequiredException.class)
    public void testSaveCourseWithNo_courseId() throws Exception {
        Course course = new Course();
        course.setCourseTitle("Database Management");
        courseServiceImpl.saveCourse(course);
    }

    /**
     * Make sure a user can't save a course with an invalid courseId (doesn't adhere to flight code XXXX-###)
     * @throws Exception
     */
    @Test(expected = InvalidCourseNumberException.class)
    public void testSaveInvalidCourseTitle() throws Exception {
        Course course = new Course();
        course.setCourseNumber("blah");
        course.setCourseTitle("Some Course Title");
        new Expectations() {{
            courseRepository.findByCourseNumber("blah");
            result = null;
        }};
        courseServiceImpl.saveCourse(course);
    }

    @Test
    public void testGetEmptyCourse() throws Exception {
        Course empty = new Course();
        Assert.assertNotNull(empty);
        Assert.assertNotNull(empty.getId());
        Assert.assertNull(empty.getCourseNumber());
        Assert.assertNull(empty.getCourseTitle());
    }

    @Test
    public void testFindCourse() throws Exception {

        new Expectations() {{
            courseRepository.findByCourseNumber("CSCI-403"); result = course1;
        }};
        Optional<Course> optional = courseServiceImpl.findCourse("CSCI-403");

        Assert.assertTrue(optional.isPresent());

        Course course = optional.get();

        assertEquals(course1, course);
    }

    @Test
    public void testDisassociateTopic() throws Exception {

        course1.setTopics(Sets.newHashSet(topic1));
        assertEquals(1, course1.getTopics().size());
        assertEquals(topic1.getDescription(), course1.getTopics().iterator().next().getDescription());

        new Expectations() {{
            courseRepository.findOne(course1.getId());
            result = course1;
            topicService.findTopic(topic1.getId());
            result = Optional.of(topic1);
        }};

        courseServiceImpl.disassociateTopic(course1.getId(), topic1.getId());

        new Verifications() {{
            Course course;
            courseRepository.save(course = withCapture());
            assertEquals(0, course.getTopics().size());
        }};
    }

    @Test
    public void testAssociateTopic() throws Exception {
        new Expectations() {{
            courseServiceImpl.findCourse(course1.getId()); result = course1;
            topicService.findTopic(topic1.getId()); result = Optional.of(topic1);
        }};
        courseServiceImpl.associateTopic(course1.getId(), topic1.getId());
        new Verifications() {{
            Course course;
            courseRepository.save(course = withCapture());
            assertEquals(1, course.getTopics().size());
            assertEquals(topic1.getId(), course.getTopics().iterator().next().getId());
        }};
    }

    @Test
    public void testGetAssociatedTopicsWildcardFilter() throws Exception {
        final UUID COURSE_ID = UUID.fromString("ae9de506-0f37-47d7-aedd-b158c1065a03");

        Course course = new Course();
        course.setId(COURSE_ID);
        course.setCourseNumber("CSCI-101");

        Topic topic = new Topic();
        topic.setDescription("Digital logic – combinational logic (MUX, DMUX)");

        course.setTopics(Sets.newHashSet(topic));

        new Expectations() {{
            courseRepository.findOne(COURSE_ID);
            result = course;
        }};

        Collection<Topic> associatedTopics = courseServiceImpl.getAssociatedTopics(COURSE_ID, "*");
        assertEquals(1, associatedTopics.size());
        assertEquals(topic.getDescription(), associatedTopics.iterator().next().getDescription());
    }

    @Test
    public void testGetAssociatedTopicsRealFilter1() throws Exception {
        final UUID COURSE_ID = UUID.fromString("ae9de506-0f37-47d7-aedd-b158c1065a03");

        Course course = new Course();
        course.setId(COURSE_ID);
        course.setCourseNumber("CSCI-101");

        Topic topic1 = new Topic();
        topic1.setDescription("Digital logic – combinational logic (MUX, DMUX)");

        Topic topic2 = new Topic();
        topic2.setDescription("Digital logic – sequential logic (Flip-flops, registers)");

        course.setTopics(Sets.newHashSet(topic1, topic2));

        new Expectations() {{
            courseRepository.findOne(COURSE_ID);
            result = course;
        }};

        Collection<Topic> associatedTopics = courseServiceImpl.getAssociatedTopics(COURSE_ID, "combinational");
        assertEquals(1, associatedTopics.size());
        assertEquals(topic1.getDescription(), associatedTopics.iterator().next().getDescription());
    }

    @Test
    public void testGetAssociatedTopicsRealFilter2() throws Exception {
        final UUID COURSE_ID = UUID.fromString("ae9de506-0f37-47d7-aedd-b158c1065a03");

        Course course = new Course();
        course.setId(COURSE_ID);
        course.setCourseNumber("CSCI-101");

        Topic topic1 = new Topic();
        topic1.setDescription("Digital logic – combinational logic (MUX, DMUX)");

        Topic topic2 = new Topic();
        topic2.setDescription("Digital logic – sequential logic (Flip-flops, registers)");

        course.setTopics(Sets.newHashSet(topic1, topic2));

        new Expectations() {{
            courseRepository.findOne(COURSE_ID);
            result = course;
        }};

        Collection<Topic> associatedTopics = courseServiceImpl.getAssociatedTopics(COURSE_ID, "sequential");
        assertEquals(1, associatedTopics.size());
        assertEquals(topic2.getDescription(), associatedTopics.iterator().next().getDescription());
    }

    @Test
    public void testGetUnassociatedCoursesWildcardFilter() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("46f64d2d-60e5-4c57-b707-4a2c2a4fced4");

        Course course = new Course();
        course.setCourseNumber("CSCI-101");
        course.setCourseTitle("Intro to Computer Science");

        new Expectations() {{
           courseRepository.findAllUnassociatedCourses(TOPIC_ID.toString());
           result = Sets.newHashSet(course);
        }};
        Collection<Course> unassociatedCourses = courseServiceImpl.getUnassociatedCourses(TOPIC_ID, "*", "*");
        assertEquals(1, unassociatedCourses.size());
        assertEquals(course.getCourseNumber(), unassociatedCourses.iterator().next().getCourseNumber());
        assertEquals(course.getCourseTitle(), unassociatedCourses.iterator().next().getCourseTitle());
    }

    @Test
    public void testGetUnassociatedCoursesRealFilter_courseNumber1() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("46f64d2d-60e5-4c57-b707-4a2c2a4fced4");

        Course course1 = new Course();
        course1.setCourseNumber("CSCI-101");
        course1.setCourseTitle("Intro to Computer Science");

        Course course2 = new Course();
        course2.setCourseNumber("CSCI-361");
        course2.setCourseTitle("Software Engineering");

        new Expectations() {{
            courseRepository.findAllUnassociatedCourses(TOPIC_ID.toString());
            result = Sets.newHashSet(course1, course2);
        }};
        Collection<Course> unassociatedCourses = courseServiceImpl.getUnassociatedCourses(TOPIC_ID, "101", "*");
        assertEquals(1, unassociatedCourses.size());
        assertEquals(course1.getCourseNumber(), unassociatedCourses.iterator().next().getCourseNumber());
        assertEquals(course1.getCourseTitle(), unassociatedCourses.iterator().next().getCourseTitle());
    }

    @Test
    public void testGetUnassociatedCoursesRealFilter_courseNumber2() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("46f64d2d-60e5-4c57-b707-4a2c2a4fced4");

        Course course1 = new Course();
        course1.setCourseNumber("CSCI-101");
        course1.setCourseTitle("Intro to Computer Science");

        Course course2 = new Course();
        course2.setCourseNumber("CSCI-361");
        course2.setCourseTitle("Software Engineering");

        new Expectations() {{
            courseRepository.findAllUnassociatedCourses(TOPIC_ID.toString());
            result = Sets.newHashSet(course1, course2);
        }};
        Collection<Course> unassociatedCourses = courseServiceImpl.getUnassociatedCourses(TOPIC_ID, "361", "*");
        assertEquals(1, unassociatedCourses.size());
        assertEquals(course2.getCourseNumber(), unassociatedCourses.iterator().next().getCourseNumber());
        assertEquals(course2.getCourseTitle(), unassociatedCourses.iterator().next().getCourseTitle());
    }

    @Test
    public void testGetUnassociatedCoursesRealFilter_courseTitle1() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("46f64d2d-60e5-4c57-b707-4a2c2a4fced4");

        Course course1 = new Course();
        course1.setCourseNumber("CSCI-101");
        course1.setCourseTitle("Intro to Computer Science");

        Course course2 = new Course();
        course2.setCourseNumber("CSCI-361");
        course2.setCourseTitle("Software Engineering");

        new Expectations() {{
            courseRepository.findAllUnassociatedCourses(TOPIC_ID.toString());
            result = Sets.newHashSet(course1, course2);
        }};
        Collection<Course> unassociatedCourses = courseServiceImpl.getUnassociatedCourses(TOPIC_ID, "*", "Computer");
        assertEquals(1, unassociatedCourses.size());
        assertEquals(course1.getCourseNumber(), unassociatedCourses.iterator().next().getCourseNumber());
        assertEquals(course1.getCourseTitle(), unassociatedCourses.iterator().next().getCourseTitle());
    }

    @Test
    public void testGetUnassociatedCoursesRealFilter_courseTitle2() throws Exception {
        final UUID TOPIC_ID = UUID.fromString("46f64d2d-60e5-4c57-b707-4a2c2a4fced4");

        Course course1 = new Course();
        course1.setCourseNumber("CSCI-101");
        course1.setCourseTitle("Intro to Computer Science");

        Course course2 = new Course();
        course2.setCourseNumber("CSCI-361");
        course2.setCourseTitle("Software Engineering");

        new Expectations() {{
            courseRepository.findAllUnassociatedCourses(TOPIC_ID.toString());
            result = Sets.newHashSet(course1, course2);
        }};
        Collection<Course> unassociatedCourses = courseServiceImpl.getUnassociatedCourses(TOPIC_ID, "*", "Engineer");
        assertEquals(1, unassociatedCourses.size());
        assertEquals(course2.getCourseNumber(), unassociatedCourses.iterator().next().getCourseNumber());
        assertEquals(course2.getCourseTitle(), unassociatedCourses.iterator().next().getCourseTitle());
    }
}
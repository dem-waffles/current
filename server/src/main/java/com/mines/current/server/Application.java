package com.mines.current.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication(scanBasePackages = "com.mines.current")
@ComponentScan({
        "com.mines.current"
})

@EnableJpaRepositories(basePackages = {
        "com.mines.current.repository"
})

@EntityScan(basePackages = {
        "com.mines.current.model"
})

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Application.class, args);
    }
}
package com.mines.current.server.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * This file serves up our static UI content from its root directory.
 */

@Configuration
public class MvcStaticResourceConfiguration extends WebMvcConfigurerAdapter {

    @Value("${ui.root.path}")
    private String uiRoot;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("file:" + uiRoot);
    }
}

package com.mines.current.server.config;

import com.mines.current.controller.Rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletResponse;

/**
 * This file configures how the UI endpoints /rest/ui/** are secured.
 */

@Order(1)
@EnableWebSecurity
@Configuration
public class UISecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(Rest.UI_URL + "/**").authenticated();
        http.formLogin()
                .loginPage("/security/login")
                .successHandler(authenticationSuccessHandler())
                .failureHandler(authenticationFailureHandler())
                .permitAll();
        http.logout()
                .logoutUrl("/security/logout")
                .logoutSuccessHandler(logoutSuccessHandler())
                .permitAll();
        http.exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint())
                .accessDeniedHandler(accessDeniedHandler());
        http.csrf().disable();
    }

    private LogoutSuccessHandler logoutSuccessHandler() {
        final Logger logger = LoggerFactory.getLogger(LogoutSuccessHandler.class);
        return (request, response, authentication) -> {
            String username = authentication != null ? authentication.getName() : "<unknown>";
            logger.info("Processed logout of user: " + username);
        };
    }

    private AccessDeniedHandler accessDeniedHandler() {
        return (request, response, exception) -> {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        };
    }

    private AuthenticationEntryPoint authenticationEntryPoint() {
        return (request, response, exception) -> {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        };
    }

    private AuthenticationSuccessHandler authenticationSuccessHandler() {
        final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessHandler.class);

        return (request, response, authentication) -> {
            logger.info("Successful login of user: " + authentication.getName());
        };
    }
    private AuthenticationFailureHandler authenticationFailureHandler() {
        final Logger logger = LoggerFactory.getLogger(AuthenticationFailureHandler.class);
        return (request, response, exception) -> {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.getMessage());
        };
    }
}

package com.mines.current.server.dev;

import com.mines.current.model.auth.User;
import com.mines.current.service.auth.UserService;
import org.h2.tools.Console;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * This file handles dev access to the app.
 *
 * A dev account with username: user, password: password
 * H2 access to whatever database we are using that is specified in the config (including native MySQL instances).
 */

@Component
@Profile("sandbox")
public class ApplicationStartup implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        Optional<User> existingDevUser = userService.getUser("user");
        if (existingDevUser.isPresent()) {
            userService.deleteUserByUsername("user");
        }
        userService.addUser(new User("user", "password"));
        final Console console = new Console();
        try {
            console.runTool("-web", "-webAllowOthers");
        } catch (Exception e) {}
    }
}

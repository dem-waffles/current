package com.mines.current.error;

public class NullCourseException extends RuntimeException {
    public NullCourseException() {
        super("Courses cannot be null!");
    }
}
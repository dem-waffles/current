package com.mines.current.error;

public class PropertyRequiredException extends RuntimeException {
    public PropertyRequiredException(String property) {
        super("Missing the following property: " + property);
    }
}

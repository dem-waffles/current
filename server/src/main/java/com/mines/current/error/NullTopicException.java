package com.mines.current.error;

public class NullTopicException extends RuntimeException {
    public NullTopicException() {
        super("Topics cannot be null!");
    }
}

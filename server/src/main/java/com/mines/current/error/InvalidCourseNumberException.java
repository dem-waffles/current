package com.mines.current.error;

public class InvalidCourseNumberException extends RuntimeException {
    public InvalidCourseNumberException(String courseNumber) {
        super("Invalid Course Number: " + courseNumber);
    }
}

package com.mines.current.error;


public class DuplicateUserException extends RuntimeException{
    public DuplicateUserException(String username) {
        super("A user with the username '" + username + "' already exists.");
    }
}

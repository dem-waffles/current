package com.mines.current.error;

public class CourseAlreadyExistsException extends RuntimeException {
    public CourseAlreadyExistsException(String courseId) {
        super("A course with courseId " + courseId + " already exists.");
    }
}

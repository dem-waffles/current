package com.mines.current.service.auth;

import com.mines.current.model.auth.User;

import javax.transaction.Transactional;
import java.util.Optional;

public interface UserService {

    @Transactional
    public Optional<User> getUser(String username);

    @Transactional
    public void deleteUserByUsername(String username);

    @Transactional
    public Optional<User> getCurrentUser();

    @Transactional
    public void addUser(User user);
}

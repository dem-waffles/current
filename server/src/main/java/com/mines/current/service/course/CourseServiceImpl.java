package com.mines.current.service.course;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.mines.current.error.*;
import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;
import com.mines.current.repository.course.CourseRepository;
import com.mines.current.service.topic.TopicService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TopicService topicService;

    @Override
    public Collection<Course> getAllCourses(String courseIdFilter, String courseTitleFilter) {
        final String finalCourseIdFilter = courseIdFilter != null ? courseIdFilter : "*";
        final String finalCourseTitleFilter = courseTitleFilter != null ? courseTitleFilter : "*";

        Collection<Course> courses = Lists.newArrayList(courseRepository.findAll());

        return courses
                .stream()
                .filter(course -> course.getCourseNumber().matches("(?i)(.*)" + finalCourseIdFilter + "(.*)"))
                .filter(course -> course.getCourseTitle().matches("(?i)(.*)" + finalCourseTitleFilter + "(.*)"))
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Course> getAllCourses() {
        return getAllCourses(null, null);
    }

    @Override
    public Course saveCourse(Course course) {
        if (course == null) {
            throw new NullCourseException();
        }

        if (StringUtils.isBlank(course.getCourseNumber())) {
            throw new PropertyRequiredException("courseId");
        }

        if (StringUtils.isBlank(course.getCourseTitle())) {
            throw new PropertyRequiredException("courseTitle");
        }

        Optional<Course> existingCourseWithSameCourseNumber = findCourse(course.getCourseNumber());
        if (existingCourseWithSameCourseNumber.isPresent()) {
            if (!existingCourseWithSameCourseNumber.get().getId().equals(course.getId())) {
                throw new CourseAlreadyExistsException(course.getCourseNumber());
            }
        }

        Optional<Course> existingCourse = findCourse(course.getId());

        if (existingCourse.isPresent()) {
            course.setTopics(existingCourse.get().getTopics());
        } else {
            course.setTopics(ImmutableList.of());
        }
        course.setCourseNumber(correctCourseNumber(course.getCourseNumber()));
        courseRepository.save(course);
        return course;
    }

    @Override
    public Optional<Course> findCourse(UUID courseId) {
        if (courseId == null) return Optional.empty();
        return Optional.ofNullable(courseRepository.findOne(courseId));
    }

    @Override
    public Optional<Course> findCourse(String courseNumber) {
        return Optional.ofNullable(courseRepository.findByCourseNumber(courseNumber));
    }

    @Override
    public void disassociateTopic(UUID courseId, UUID topicId) {
        Course course = findCourse(courseId)
                .orElseThrow(() -> new CourseNotFoundException(courseId));
        Topic topic = topicService.findTopic(topicId)
                .orElseThrow(() -> new TopicNotFoundException(topicId));
        course.removeTopic(topic);
        courseRepository.save(course);
    }

    @Override
    public void associateTopic(UUID courseId, UUID topicId) {
        Course course = findCourse(courseId)
                .orElseThrow(() -> new CourseNotFoundException(courseId));
        Topic topic = topicService.findTopic(topicId)
                .orElseThrow(() -> new TopicNotFoundException(topicId));
        course.addTopic(topic);
        courseRepository.save(course);
    }

    @Override
    public Collection<Topic> getAssociatedTopics(UUID courseId, String filter) {
        Optional<Course> course = findCourse(courseId);

        if (!course.isPresent()) {
            return ImmutableList.of();
        }

        Collection<Topic> topics = Lists.newArrayList(course.get().getTopics());
        if (filter == null) {
            topics
                    .stream()
                    .sorted()
                    .collect(Collectors.toList());
        }
        return topics
                .stream()
                .filter(topic -> topic.getDescription().matches("(?i)(.*)" + filter + "(.*)"))
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Course> getUnassociatedCourses(UUID topicId, String courseNumberFilter, String courseTitleFilter) {
        return courseRepository.findAllUnassociatedCourses(topicId.toString())
                .stream()
                .filter(course -> course.getCourseNumber().matches("(?i)(.*)" + courseNumberFilter + "(.*)"))
                .filter(course -> course.getCourseTitle().matches("(?i)(.*)" + courseTitleFilter + "(.*)"))
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public void deleteCourse(Course course) {
        if (!findCourse(course.getId()).isPresent()) {
            throw new CourseNotFoundException(course.getId());
        }
        courseRepository.delete(course);
    }

    public String correctCourseNumber(String courseNumber) {
        if (!courseNumber.contains("-")) {
            throw new InvalidCourseNumberException(courseNumber);
        }
        String[] split = courseNumber.split("-");
        return split[0].toUpperCase() + "-" + split[1];
    }
}

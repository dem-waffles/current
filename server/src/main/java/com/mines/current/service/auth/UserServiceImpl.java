package com.mines.current.service.auth;

import com.mines.current.error.DuplicateUserException;
import com.mines.current.model.auth.User;
import com.mines.current.repository.auth.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public void addUser(User user) {
        Optional<User> existingUser = getUser(user.getUsername());
        if (existingUser.isPresent()) {
            throw new DuplicateUserException(user.getUsername());
        }
        userRepository.save(user);
    }

    @Override
    public void deleteUserByUsername(String username) {
        Optional<User> user = getUser(username);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException(username);
        }
        userRepository.delete(user.get());
    }

    public Optional<User> getUser(String username) {
        if (username == null) {
            throw new RuntimeException("username cannot be null");
        }
        User user = userRepository.findByUsername(username);

        if (user == null) {
            return Optional.empty();
        }

        return Optional.of(user);
    }

    @Override
    public Optional<User> getCurrentUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        UserDetails details = (UserDetails) authentication.getPrincipal();
        return getUser(details.getUsername());
    }
}

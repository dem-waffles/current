package com.mines.current.service.course;

import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;

import java.util.*;

public interface CourseService {

    public Collection<Course> getAllCourses(String courseIdFilter, String courseTitleFilter);

    public Collection<Course> getAllCourses();

    public Course saveCourse(Course course);

    public Optional<Course> findCourse(UUID courseId);

    public Optional<Course> findCourse(String courseId);

    public void disassociateTopic(UUID courseId, UUID topicId);

    public void associateTopic(UUID courseId, UUID topicId);

    public Collection<Topic> getAssociatedTopics(UUID courseId, String filter);

    public Collection<Course> getUnassociatedCourses(UUID topicId, String courseIdFilter, String courseTitleFilter);

    public void deleteCourse(Course course);
}


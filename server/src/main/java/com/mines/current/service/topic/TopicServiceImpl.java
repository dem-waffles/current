package com.mines.current.service.topic;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.mines.current.error.CourseNotFoundException;
import com.mines.current.error.NullTopicException;
import com.mines.current.error.PropertyRequiredException;
import com.mines.current.error.TopicNotFoundException;
import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;
import com.mines.current.repository.topic.TopicRepository;
import com.mines.current.service.course.CourseService;
import com.mines.current.service.course.CourseServiceImpl;
import mockit.Injectable;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private CourseService courseService;

    @Override
    public Collection<Topic> getAllTopics(String filter) {
        final String usedFilter = filter != null ? filter : "*";

        Collection<Topic> topics = Lists.newArrayList(topicRepository.findAll());

        return topics
                .stream()
                .filter(topic -> topic.getDescription().matches("(?i)(.*)" + usedFilter + "(.*)"))
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Topic> getAllTopics() {
        return getAllTopics(null);
    }

    @Override
    public Topic saveTopic(Topic topic) {
        if (topic == null) {
            throw new NullTopicException();
        }

        if (StringUtils.isBlank(topic.getDescription())) {
            throw new PropertyRequiredException("description");
        }

        Optional<Topic> existingTopicWithSameDescription = findTopic(topic.getDescription());

        if (existingTopicWithSameDescription.isPresent()) {
            if (!existingTopicWithSameDescription.get().getId().equals(topic.getId())) {
                throw new RuntimeException("A topic with description " + topic.getDescription() + " already exists!");
            }
        }

        Optional<Topic> existingTopic = findTopic(topic.getId());
        if (existingTopic.isPresent()) {
            topic.setCourses(existingTopic.get().getCourses());
        } else {
            topic.setCourses(ImmutableList.of());
        }
        topicRepository.save(topic);
        return topic;
    }

    @Override
    public Optional<Topic> findTopic(UUID topicId) {
        return Optional.ofNullable(topicRepository.findOne(topicId));
    }

    @Override
    public Optional<Topic> findTopic(String topicDescription) {
        return Optional.ofNullable(topicRepository.findTopicByDescription(topicDescription));
    }

    @Override
    public void disassociateCourse(UUID topicId, UUID courseId) {
        Topic topic = findTopic(topicId)
                .orElseThrow(() -> new TopicNotFoundException(topicId));
        Course course = courseService.findCourse(courseId)
                .orElseThrow(() -> new CourseNotFoundException(courseId));
        topic.removeCourse(course);
        topicRepository.save(topic);
    }

    @Override
    public void associateCourse(UUID topicId, UUID courseId) {
        Topic topic = findTopic(topicId)
                .orElseThrow(() -> new TopicNotFoundException(topicId));
        Course course = courseService.findCourse(courseId)
                .orElseThrow(() -> new CourseNotFoundException(courseId));
        topic.addCourse(course);
        topicRepository.save(topic);
    }

    @Override
    public Collection<Course> getAssociatedCourses(UUID topicId, String courseIdFilter, String courseTitleFilter) {
        if (courseIdFilter == null && courseTitleFilter == null) {
            return findTopic(topicId)
                    .orElseThrow(() -> new TopicNotFoundException((topicId)))
                    .getCourses();
        }
        Collection<Course> courses = Lists.newArrayList(findTopic(topicId)
                .orElseThrow(() -> new TopicNotFoundException(topicId))
                .getCourses());
        return courses
                .stream()
                .filter(course -> course.getCourseNumber().matches("(?i)(.*)" + courseIdFilter + "(.*)"))
                .filter(course -> course.getCourseTitle().matches("(?i)(.*)" + courseTitleFilter + "(.*)"))
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public Collection<Topic> getAllUnassociatedTopics(UUID courseId, String filter) {
        return topicRepository.findAllUnassociatedTopics(courseId.toString())
                .stream()
                .filter(topic -> topic.getDescription().matches("(?i)(.*)" + filter + "(?i)(.*)"))
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public void deleteTopic(Topic topic) {
        if (!findTopic(topic.getId()).isPresent()) {
            throw new TopicNotFoundException(topic.getId());
        }
        topicRepository.delete(topic);
    }
}

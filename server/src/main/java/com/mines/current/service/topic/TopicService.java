package com.mines.current.service.topic;

import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface TopicService {
    public Collection<Topic> getAllTopics(String filter);
    public Collection<Topic> getAllTopics();

    public Topic saveTopic(Topic topic);

    public Optional<Topic> findTopic(UUID topicId);
    public Optional<Topic> findTopic(String topicDescription);

    public void disassociateCourse(UUID topicId, UUID courseId);
    public void associateCourse(UUID topicId, UUID courseId);

    public Collection<Course> getAssociatedCourses (UUID topicId, String courseIdFilter, String courseTitleFilter);

    public Collection<Topic> getAllUnassociatedTopics(UUID courseId, String filter);

    public void deleteTopic(Topic topic);
}

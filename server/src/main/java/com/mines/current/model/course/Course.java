package com.mines.current.model.course;

import com.mines.current.model.topic.Topic;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

@Entity(name = "course")
@Table(name = "course")
public class Course implements Comparable<Course> {
	@Id
	@Column(name = "id", length = 36)
    @Type(type = "uuid-char")
	private UUID id;

	@Column(name = "course_number", unique = true)
	private String courseNumber;

	@Column(name = "course_title")
	private String courseTitle;

	@ManyToMany
	@JoinTable(
			name = "course_topic",
			joinColumns = @JoinColumn(name = "course_id"),
			inverseJoinColumns = @JoinColumn(name = "topic_id")
	)
	private Collection<Topic> topics;

	public Course() {
		id = id != null ? id : UUID.randomUUID();
		topics = new HashSet<>();
	}

    public Course(UUID id) {
        this.id = id;
        topics = new HashSet<>();
    }

	public UUID getId() {
		return id;
	}

	public String getCourseNumber() {
		return courseNumber;
	}

	public void setCourseNumber(String courseNumber) {
		this.courseNumber = courseNumber;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public void addTopic(Topic topic) {
		topics.add(topic);
	}

	public void removeTopic(Topic topic) {
		topics.remove(topic);
	}

	public Collection<Topic> getTopics() {
		Collection<Topic> retval = new HashSet<>();
		for (Topic topic : topics) {
			Topic t = new Topic();
			t.setId(topic.getId());
			t.setDescription(topic.getDescription());
			retval.add(t);
		}
		return retval;
	}

    public void setTopics(Collection<Topic> topics) {
        this.topics = topics;
    }

    @Override
	public int compareTo(Course o) {
		return courseTitle.compareTo(o.getCourseTitle());
	}

	public void setId(UUID id) {
		this.id = id;
	}
}

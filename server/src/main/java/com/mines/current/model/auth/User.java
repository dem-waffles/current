package com.mines.current.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "account")
public class User {

    public static final int USERNAME_MAX_SIZE = 50;
    public static final int PASSWORD_MAX_SIZE = 50;

    @Id
    @Column(name = "id", length = 36)
    @Type(type = "uuid-char")
    UUID id;

    @Column(name = "username",
            length = USERNAME_MAX_SIZE,
            unique = true)
    private String username;

    @JsonIgnore
    @Column(name = "password", length = PASSWORD_MAX_SIZE)
    private String password;

    public User() {}

    public User(String username, String password) {
        this.id = UUID.randomUUID();
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getId() {
        return id;
    }
}

package com.mines.current.model.topic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mines.current.model.course.Course;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

@Entity(name = "topic")
@Table(name = "topic")
public class Topic  implements Comparable<Topic> {
	@Id
	@Column(name = "id", length = 36)
    @Type(type = "uuid-char")
	private UUID id;

	@Column(name = "description")
	private String description;

	@ManyToMany
	@JoinTable(
			name = "course_topic",
			joinColumns = @JoinColumn(name = "topic_id"),
			inverseJoinColumns = @JoinColumn(name = "course_id")
	)
	private Collection<Course> courses;

	public Topic() {
		id = id != null ? id :UUID.randomUUID();
		courses = new HashSet<>();
	}

	public UUID getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void addCourse(Course course) {
		this.courses.add(course);
	}

	public void removeCourse(Course course) {
		courses.remove(course);
	}

	public Collection<Course> getCourses() {
		Collection<Course> retval = new HashSet<>();
		for (Course course : courses) {
			Course c = new Course();
			c.setId(course.getId());
			c.setCourseNumber(course.getCourseNumber());
			c.setCourseTitle(course.getCourseTitle());
			retval.add(c);
		}
		return retval;
	}

    public void setCourses(Collection<Course> courses) {
        this.courses = courses;
    }

    @Override
	public int compareTo(Topic o) {
		return description.compareTo(o.getDescription());
	}

	public void setId(UUID id) {
		this.id = id;
	}
}

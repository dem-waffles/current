package com.mines.current.controller.auth;

import com.mines.current.controller.Rest;
import com.mines.current.error.CurrentUserNotFoundException;
import com.mines.current.model.auth.User;
import com.mines.current.service.auth.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    private static final String AUTH_URL = "/auth";
    private static final String CURRENT_USER_URL = AUTH_URL + "/currentUser";

    @Autowired
    private UserService userService;

    @RequestMapping({Rest.UI_URL + CURRENT_USER_URL, Rest.API_URL + CURRENT_USER_URL})
    public User getCurrentUser() {
        return userService.getCurrentUser()
                .orElseThrow(() -> new CurrentUserNotFoundException());
    }
}

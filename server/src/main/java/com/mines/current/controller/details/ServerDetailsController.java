package com.mines.current.controller.details;

import com.mines.current.controller.Rest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServerDetailsController {
    private class ServerDetails {
        private String[] profiles;
        public void setProfiles(String[] profiles) {
            this.profiles = profiles;
        }
        public String[] getProfiles() {
            return profiles;
        }
    }
    @Autowired
    private Environment environment;
    public static final String SERVER_DETAILS_URL = "/serverDetails";

    @RequestMapping({Rest.UI_URL + SERVER_DETAILS_URL, Rest.API_URL + SERVER_DETAILS_URL})
    public ServerDetails getServerDetails() {
        ServerDetails serverDetails = new ServerDetails();
        environment.getActiveProfiles();
        serverDetails.setProfiles(environment.getActiveProfiles());
        return serverDetails;
    }
}

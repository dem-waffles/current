package com.mines.current.controller.topic;

import com.mines.current.controller.Rest;
import com.mines.current.error.TopicNotFoundException;
import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;
import com.mines.current.service.topic.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.UUID;

@RestController
public class TopicController {

    public static final String TOPIC_URL = "/topic";

    public static final String ID_URL = TOPIC_URL + "/{topicId}";

    public static final String GET_COURSES_URL = ID_URL + "/courses";

    public static final String DISASSOCIATE_URL = ID_URL + "/disassociate/{courseNumber}";

    public static final String ASSOCIATE_URL = ID_URL + "/associate/{courseNumber}";

    public static final String UNASSOCIATED_COURSES_URL = TOPIC_URL + "/unassociated";

    @Autowired
    private TopicService topicService;

    @RequestMapping({Rest.UI_URL + TOPIC_URL, Rest.API_URL + TOPIC_URL})
    public Collection<Topic> getAllTopics(HttpServletRequest httpServletRequest) {
        String filter = httpServletRequest.getParameter("filter");
        return topicService.getAllTopics(filter);
    }

    @RequestMapping({Rest.UI_URL + ID_URL, Rest.API_URL + ID_URL})
    public Topic getTopic(@PathVariable UUID topicId) {
        return topicService.findTopic(topicId)
                .orElseThrow(() -> new TopicNotFoundException(topicId));
    }

    @RequestMapping(value = {Rest.UI_URL + TOPIC_URL, Rest.API_URL + TOPIC_URL}, method = RequestMethod.POST)
    public Topic saveTopic(@RequestBody(required = false) Topic topic) {
        return topicService.saveTopic(topic);
    }

    @RequestMapping({Rest.UI_URL + GET_COURSES_URL, Rest.API_URL + GET_COURSES_URL})
    public Collection<Course> getCourses(@PathVariable UUID topicId, @RequestParam String courseNumberFilter, @RequestParam String courseTitleFilter) {
        return topicService.getAssociatedCourses(topicId, courseNumberFilter, courseTitleFilter);
    }

    @RequestMapping({Rest.UI_URL + UNASSOCIATED_COURSES_URL, Rest.API_URL + UNASSOCIATED_COURSES_URL})
    public Collection<Topic> getUnassociatedTopics(@RequestParam UUID courseId, @RequestParam String filter) {
        return topicService.getAllUnassociatedTopics(courseId, filter);
    }

    @RequestMapping(value = {Rest.UI_URL + DISASSOCIATE_URL, Rest.API_URL + DISASSOCIATE_URL}, method = RequestMethod.POST)
    public void disassociate(@PathVariable UUID topicId, @PathVariable  UUID courseNumber) {
        topicService.disassociateCourse(topicId, courseNumber);
    }

    @RequestMapping(value = {Rest.UI_URL + ASSOCIATE_URL, Rest.API_URL + ASSOCIATE_URL}, method = RequestMethod.POST)
    public void associate(@PathVariable UUID topicId, @PathVariable UUID courseNumber) {
        topicService.associateCourse(topicId, courseNumber);
    }

    @RequestMapping(value = {Rest.UI_URL + TOPIC_URL, Rest.API_URL + TOPIC_URL}, method = RequestMethod.DELETE)
    public void deleteTopic(@RequestBody Topic topic) {
        topicService.deleteTopic(topic);
    }

}

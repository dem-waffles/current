package com.mines.current.controller.course;

import com.mines.current.controller.Rest;
import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;
import com.mines.current.service.course.CourseService;
import com.mines.current.error.CourseNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.UUID;

@RestController
public class CourseController {
    public static final String COURSE_URL = "/course";

    public static final String ID_URL = COURSE_URL + "/{courseNumber}";

    public static final String GET_TOPICS_URL = ID_URL + "/topics";

    public static final String DISASSOCIATE_URL = ID_URL + "/disassociate/{topicId}";

    public static final String ASSOCIATE_URL = ID_URL + "/associate/{topicId}";

    public static final String GET_UNASSOCIATED_TOPICS_URL = COURSE_URL + "/unassociated";

    @Autowired
    private CourseService courseService;

    @RequestMapping({Rest.UI_URL + COURSE_URL, Rest.API_URL + COURSE_URL})
    public Collection<Course> getAllCourses(HttpServletRequest httpServletRequest) {
        String courseNumberFilter = httpServletRequest.getParameter("courseNumberFilter");
        String courseTitleFilter = httpServletRequest.getParameter("courseTitleFilter");
        return courseService.getAllCourses(courseNumberFilter, courseTitleFilter);
    }

    @RequestMapping({Rest.UI_URL + ID_URL, Rest.API_URL + ID_URL})
    public Course getCourse(@PathVariable UUID courseNumber) {
        return courseService.findCourse(courseNumber)
                .orElseThrow(() -> new CourseNotFoundException(courseNumber));
    }

    @RequestMapping(value = {Rest.UI_URL + COURSE_URL, Rest.API_URL + COURSE_URL}, method = RequestMethod.POST)
    public Course saveCourse(@RequestBody(required = false) Course course) {
        return courseService.saveCourse(course);
    }

    @RequestMapping({Rest.UI_URL + GET_TOPICS_URL, Rest.API_URL + GET_TOPICS_URL})
    public Collection<Topic> getTopics(@PathVariable UUID courseNumber, @RequestParam String filter) {
        return courseService.getAssociatedTopics(courseNumber, filter);
    }

    @RequestMapping(value = {Rest.UI_URL + DISASSOCIATE_URL, Rest.API_URL + DISASSOCIATE_URL}, method = RequestMethod.POST)
    public void disassociate(@PathVariable UUID courseNumber, @PathVariable  UUID topicId) {
        courseService.disassociateTopic(courseNumber, topicId);
    }

    @RequestMapping(value = {Rest.UI_URL + ASSOCIATE_URL, Rest.API_URL + ASSOCIATE_URL}, method = RequestMethod.POST)
    public void associate(@PathVariable UUID courseNumber, @PathVariable UUID topicId) {
        courseService.associateTopic(courseNumber, topicId);
    }

    @RequestMapping({Rest.UI_URL + GET_UNASSOCIATED_TOPICS_URL, Rest.API_URL + GET_UNASSOCIATED_TOPICS_URL})
    public Collection<Course> getUnassociatedCourses(@RequestParam UUID topicId, @RequestParam String courseNumberFilter, @RequestParam String courseTitleFilter) {
        return courseService.getUnassociatedCourses(topicId, courseNumberFilter, courseTitleFilter);
    }

    @RequestMapping(value = {Rest.UI_URL + COURSE_URL, Rest.API_URL + COURSE_URL}, method = RequestMethod.DELETE)
    public void deleteCourse(@RequestBody Course course) {
        courseService.deleteCourse(course);
    }

}

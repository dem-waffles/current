package com.mines.current.controller;

public interface Rest {
    public static final String ROOT_URL = "/rest";
    public static final String UI_URL = ROOT_URL + "/ui";
    public static final String API_URL = ROOT_URL + "/api";
}

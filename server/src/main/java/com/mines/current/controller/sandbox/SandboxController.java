package com.mines.current.controller.sandbox;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mines.current.controller.Rest;
import com.mines.current.model.course.Course;
import com.mines.current.model.topic.Topic;
import com.mines.current.repository.course.CourseRepository;
import com.mines.current.repository.topic.TopicRepository;
import com.mines.current.service.course.CourseService;
import com.mines.current.service.topic.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Profile("sandbox")
@RestController
public class SandboxController {

    public static final String SANDBOX_URL = "/sandbox";
    public static final String WIPE_DATA_URL = SANDBOX_URL + "/wipe";
    public static final String POPULATE_DATA_URL = SANDBOX_URL + "/populate";

    @Value("${sandbox.courses}")
    private String coursesJSONFile;

    @Value("${sandbox.topics}")
    private String topicsJSONFile;

    @Value("${sandbox.course_topic}")
    private String courseTopicJSONFile;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private CourseService courseService;

    @Autowired
    private TopicService topicService;

    @RequestMapping(value = {Rest.UI_URL + WIPE_DATA_URL, Rest.API_URL + WIPE_DATA_URL}, method = RequestMethod.POST)
    @Transactional
    public void wipeData() {
        courseRepository.deleteAll();
        topicRepository.deleteAll();
    }

    @RequestMapping(value = {Rest.UI_URL + POPULATE_DATA_URL, Rest.API_URL + POPULATE_DATA_URL}, method = RequestMethod.POST)
    @Transactional
    public void populateData() throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        Course[] courses = mapper.readValue(new File(coursesJSONFile), Course[].class);
        courseRepository.deleteAll();
        courseRepository.save(Arrays.asList(courses));

        Topic[] topics = mapper.readValue(new File(topicsJSONFile), Topic[].class);
        topicRepository.deleteAll();
        topicRepository.save(Arrays.asList(topics));

        List<List<String>> associations = mapper.readValue(new File(courseTopicJSONFile), List.class);

        for (List<String> association : associations) {
            Course course = courseService.findCourse(association.get(1)).get();
            Topic topic = topicService.findTopic(association.get(0)).get();
            course.addTopic(topic);
            courseRepository.save(course);
        }
    }
}

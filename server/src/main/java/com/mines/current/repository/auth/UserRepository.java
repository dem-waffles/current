package com.mines.current.repository.auth;

import com.mines.current.model.auth.User;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface UserRepository extends CrudRepository<User, UUID> {
    public User findByUsername(String username);
}

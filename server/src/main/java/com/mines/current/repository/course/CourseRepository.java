package com.mines.current.repository.course;

import com.mines.current.model.course.Course;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.UUID;

@Repository
public interface CourseRepository extends CrudRepository<Course, UUID> {
    public Course findByCourseNumber(String courseNumber);

    @Query(value = "SELECT * FROM course WHERE id NOT IN (SELECT course_id FROM course_topic WHERE topic_id = ?1)", nativeQuery = true)
    public Collection<Course> findAllUnassociatedCourses(@Param("topic_id") String topicId);
}
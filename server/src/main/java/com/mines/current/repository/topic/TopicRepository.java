package com.mines.current.repository.topic;

import com.mines.current.model.topic.Topic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.UUID;

@Repository
public interface TopicRepository extends CrudRepository<Topic, UUID> {
    Topic findTopicByDescription(String topicDescription);

    @Query(value = "SELECT * FROM topic WHERE id NOT IN (SELECT topic_id FROM course_topic WHERE course_id = ?1)", nativeQuery = true)
    Collection<Topic> findAllUnassociatedTopics(@Param("course_id") String courseId);
}
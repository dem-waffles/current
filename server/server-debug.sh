#!/bin/bash

pushd $(dirname $0)

java -jar target/dist/server/current-0.1.0.jar --spring.config.location=file:config/$(whoami)/application-$(whoami).yml

popd

# !/bin/bash

pushd $(dirname $0)

#
# TODO
# Define some variables up here so everything isn't hard-coded.

function startServer() {
    nohup java -jar ./current-0.1.0.jar --spring.config.location=file:config/prod/application-prod.yml &
    # print the process pid to this file so we can kill it later by pid reference.
    echo "$!" > "./.server.pid"
}

function stopServer() {
    cat "./.server.pid" | xargs kill
    rm "./.server.pid"
}

if [ $# -eq 0 ] ; then
    echo "start or stop"
    exit 1
fi

case $1 in
    start) startServer;;

    stop) stopServer;;

    *) echo "start or stop" && exit 1
esac

popd
